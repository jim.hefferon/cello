// cellomapjh.asy
//  Map of cello
// Include the anchors, a few positions, and a few harmonics

import settings;
settings.outformat="pdf";
settings.render=0;

texpreamble("\usepackage{amsmath}\usepackage[sfdefault]{cabin}\usepackage[T1]{fontenc}");

unitsize(1pt);

string OUTPUT_FN = "cellomapjh%02d";

real PAGE_LENGTH = 4inch;

// https://color.adobe.com/asus-echelon-color-theme-10386476/edit/?copy=true
pen darkgrey = rgb(91/255, 103/255, 114/255);
pen lightgrey = rgb(142/255, 151/255, 163/255);
pen yellow = rgb(244/255, 239/255, 190/255);
pen white = rgb(241/255, 241/255, 241/255);
pen black = rgb(13/255, 11/255, 11/255);

real STRINGPEN_WIDTH=6pt;
pen STRINGPEN = linewidth(STRINGPEN_WIDTH)+lightgray;
pen NOTEBORDERPEN = linewidth(0.4pt)+darkgrey;
pen NOTEFILLPEN = yellow;

// Make the note spacing correct
real TWELTH_ROOT_OF_TWO = exp(log(2)/12);
int NUM_FRETS = 20;
real[] fret_dist;        // distance from fret to fret
real[] fret_dist_total;  // fret_dist_total[fret] = dist from nut to fret
fret_dist[0] = 1;
fret_dist_total[0] = 1;
for (int fret=1; fret<=NUM_FRETS; ++fret) {  // one extra so can extrapolate
  fret_dist[fret] = fret_dist[fret-1] * (1/TWELTH_ROOT_OF_TWO);
  fret_dist_total[fret] =  fret_dist_total[fret-1]+fret_dist[fret];
}

// Notes; change to sharps if you like
string[] doremi = {"A", "B$\flat$", "B", "C", "C$\sharp$", "D", "E$\flat$",
		   "E", "F", "F$\sharp$", "G", "A$\flat$"};
doremi.cyclic=true;

// notename
//  Determine formatting of the name of the notes
// notedex  index of note name in doremi
// fontsize  size of font
string notename(int notedex, int fontsize=20) {
  return "\fontsize{"+format("%d",fontsize)+"}{14}\selectfont "+doremi[notedex]+"";
}

real CIRCLE_RAD =0.50; // radius of circle for notes
// Draw one note position
//   pic  picture on which to draw
//   notefirstindex  index into doremi of open string note
//   fret  which fret is this on the string?
void note(picture pic=currentpicture, int firstnoteindex, int fret, pair position, bool openstring=false) {
  int notedex = firstnoteindex+fret;
  if (openstring==false) { // draw circle or square
    if (fret==2  // anchors; draw square
	|| fret==5
	|| fret==7
	|| fret==10
	|| fret==12
	|| fret==17
	|| fret==19){
      filldraw(pic, box(position-0.9*(CIRCLE_RAD,CIRCLE_RAD),position+0.9*(CIRCLE_RAD,CIRCLE_RAD)), drawpen=NOTEBORDERPEN, fillpen=NOTEFILLPEN);
      label(pic, notename(notedex, 12), position);
    } else { // non-anchor; draw circle
      filldraw(pic, circle(position,CIRCLE_RAD), drawpen=NOTEBORDERPEN, fillpen=NOTEFILLPEN);
      label(pic, notename(notedex, 12), position);
    }
  } else {  // open string name is without circle or square
    label(pic, notename(notedex, 16), position-(0,0.5));
  }
}

int[] stringindices = {3, 10, 5, 0};  // doremi index of string's first note

int firstnoteindex(int dex) {
  return stringindices[dex];
}


// Give the y value (from the start of the string)
// return where to put the note on this string
real yposn(real y) { // up to down
  int fl=floor(y);
  real r=y-fl;
  real s=fret_dist_total[fl]+r*(fret_dist_total[fl+1]-fret_dist_total[fl]);
  return -3*s+3;  // up to down makes this negative
}
// Given x value, where to put the note?
real xposn(real x) {
  return 2*x;
}


// ============== Draw the picture ================
picture pic;
int picnum = 0;
unitsize(pic,0.715cm);

// draw the cello body
pair cblf = (xposn(-1),yposn(10)-0.55);
pair cbrt = (xposn(4),yposn(10)-0.55);
path shoulder = cblf{(1,0.25)}..{(1,-0.25)}cbrt;
draw(pic,shoulder,darkgray);

// Put in the words about positions, harmonics
pen POSITION_PEN=black+fontsize(10); 
label(pic,"\makebox[0em][r]{$\text{1}^{\text{st}}$ position}",(xposn(-0.5),yposn(2)),POSITION_PEN);
label(pic,"\makebox[0em][r]{$\text{2}^{\text{nd}}$ position}",(xposn(-0.5),yposn(4)),POSITION_PEN);
label(pic,"\makebox[0em][r]{$\text{3}^{\text{rd}}$ position}",(xposn(-0.5),yposn(5)),POSITION_PEN);
label(pic,"\makebox[0em][r]{$\text{4}^{\text{th}}$ position}",(xposn(-0.5),yposn(7)),POSITION_PEN);
label(pic,"\makebox[0em][r]{$\text{5}^{\text{th}}$ position}",(xposn(-0.5),yposn(9)),POSITION_PEN);
label(pic,"\makebox[0em][r]{$\text{6}^{\text{th}}$ position}",(xposn(-0.5),yposn(10)),POSITION_PEN);


label(pic,"\makebox[0em][l]{Second harmonic}",(xposn(3.5),yposn(12)),POSITION_PEN);
label(pic,"\makebox[0em][l]{Third harmonic}",(xposn(3.5),yposn(7)),POSITION_PEN);
label(pic,"\makebox[0em][l]{Third harmonic}",(xposn(3.5),yposn(19)),POSITION_PEN);
label(pic,"\makebox[0em][l]{Fourth harmonic}",(xposn(3.5),yposn(5)),POSITION_PEN);


// draw the strings
for(int i=0; i<=3; ++i){
  draw(pic,
       (xposn(i),yposn(0.40)) -- (xposn(i),yposn(NUM_FRETS-0.5)),
       STRINGPEN+squarecap);
    }
// draw the nut
draw(pic, (xposn(0),yposn(0.40))--(xposn(3),yposn(0.40)), STRINGPEN+extendcap);
    
// Draw the notes, over the strings
bool openstring=false;  // is this note an openstring?
for(int i=0; i<=3; ++i){
  for(int fret=0; fret<NUM_FRETS; ++fret){
    // write(format("fret=%d\n",fret));
    if (fret==0) {
      openstring=true;
    } else {
      openstring=false;
    }
    note(pic,
	 firstnoteindex(i),
	 fret,
	 (xposn(i), yposn(fret)),
	 openstring);
  }
}
// Get the printing in the picture
dot(pic,(-5,1),invisible);
dot(pic,(12,1),white);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





