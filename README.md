# Summary

Map of the finger positions on the cello.


# License

Creative Common Share Alike license.


# Version

2017-Dec-28  Jim Hefferon



# Repo
https://gitlab.com/jim.hefferon/cellomap



